
// #2 CREATE NEW COLLECTION OF USERS
db.usercollections.insertMany([
	{
	"firstName": "Stephen",
	"lastName": "Hawking",
	"age": 76, 
	"email": "stephenhawking@mail.com", 
	"department": "HR"
	},
	{
	"firstName": "Neil",
	"lastName": "Armstrong",
	"age": 82, 
	"email": "neilarmstrong@mail.com", 
	"department": "HR"
	}, 
	{
	"firstName": "Bill",
	"lastName": "Gates",
	"age": 65, 
	"email": "billgates@mail.com", 
	"department": "Operations"
	}, 
	{
	"firstName": "Jane",
	"lastName": "Doe",
	"age": 21, 
	"email": "janedoe@mail.com", 
	"department": "HR"
	}
])

// #3 FIND USERS WITH LETTER S & D
db.usercollections.find({
	$or: [
		{"firstName": {
			$regex: 's', 
			$options: '$i'
			}},
		{"lastName": {
			$regex: 'd',
			$options: '$i'
		}}
		]}, 
		{
		"_id": 0,
		"firstName": 1,
		"lastName": 1, 
	})

// #4 HR DEPARTMENT AGE GREATER THAN OR EQUAL TO 70
db.usercollections.find({
	$and: [
	{ 
		"department": "HR"
	}, 
	{
		"age": {$gte: 70}
	}
	]
})

// #5 FIRST NAME WITH E & AGE LESS THAN OR EQUAL TO 30
db.usercollections.find({
	$and: [
	{"firstName": {
		$regex: 'e', 
		$options: 'i'
	}}, 
	{
		"age": {$lte: 30}
	}
	]
})

// END